function createCard(title, description, pictureUrl, cardDate, locationName) {
    return `
    <div class="col-4">
      <div class="p-2 g-col-6 shadow-lg mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-secondary">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-transparent border-success">
            ${cardDate}
        </div>
      </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    // defining url which is where we will fetch
    const url = 'http://localhost:8000/api/conferences/';


    try {
        // try to use fetch
        // returns a promise that will resolve to the response of the request
        // await pauses further execution of the code until the promise resolves
        // resolved response is assigned to the variable 'response'
        const response = await fetch(url);
        // if conditional checks to see if response was a sucess
        if (!response.ok) {
            // throw error if it doesnt work
            throw new Error ('Network response was not OK: error, bad, very bad, something server');
        } else {
            // if works; extract json from response, assign it to variable 'data'
            const data = await response.json();
            // looping over conference details
            for (let conference of data.conferences) {
                // conference detail url
                const detailUrl = `http://localhost:8000${conference.href}`;
                // recieving detail json
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    // taking json extracting it from the response and turning it into javascript
                    const details = await detailResponse.json();
                    // detail view of conference
                    const conferenceDetail = details.conference;
                    // get title
                    const title = conferenceDetail.name;
                    // looks for the first occurance of a css class 'card-title' and if found is set as variable nameTag
                    // the dot in card-title is used to target elements by class name
                    // const nameTitle = document.querySelector('.card-title');
                    // assigns the conference name to the html
                    // nameTitle.innerHTML = conference.name;
                    // conference description
                    const description = conferenceDetail.description;
                    // const nameText = document.querySelector(".card-text");
                    // nameText.innerHTML = conferenceDetail.description;
                    // image url
                    const pictureUrl = conferenceDetail.location.picture_url;

                    const locationName = conferenceDetail.location.name;

                    const date = new Date(conferenceDetail.starts);
                    const cardDate = date.toLocaleDateString();

                    // creating html card???
                    const html = createCard(title, description, pictureUrl, cardDate, locationName);

                    // html access look up insert adjecent html in more detail******
                    // const imageTag = document.querySelector(".card-img-top");
                    // imageTag.src = imageUrl;
                    // console.log(html);
                    // create html access
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                    // console.log(conferenceDetail)
                }
            }
        }
        // catch handles errors
    } catch (error) {
        console.error('error: some sort of problem with the fetch operation', error);
    }

});
