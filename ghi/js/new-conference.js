window.addEventListener("DOMContentLoaded", async () => {
// get location for form
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const locations = data.locations;
        const selectTag = document.getElementById("location");
        // console.log(data.locations);
        for (const location of locations) {
            const option = document.createElement("option");
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
            // console.log(location, location.name, location)
        }
    }
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = "http://localhost:8000/api/conferences/"
        // console.log("test:   ",json)
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                "Content-Type": "application/json",
            },
        };
        // console.log(conferenceUrl, fetchConfig)
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        }
    });
});
