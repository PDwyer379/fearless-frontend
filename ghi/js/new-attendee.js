window.addEventListener("DOMContentLoaded",  async () => {
    const selectTag = document.getElementById("conference");
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        // console.log(data)
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
            console.log(conference)
        };
        // const select = document.getElementById('conference');
        selectTag.classList.remove('d-none');

        const loading = document.getElementById("loading-conference-spinner");
        loading.classList.add("d-none");
        // get attendee form element by id
        const formTag = document.getElementById("create-attendee-form");
        // add event handler for the submit
        formTag.addEventListener("submit", async event => {
            // prevent default from occuring
            event.preventDefault();
            // create formData
            const formData = new FormData(formTag);
            // get new oject from formData... jsonify it?
            const json = JSON.stringify(Object.fromEntries(formData));
            // create options for the fetch ...url and config
            const conferenceHref = formData.get("conference");
            console.log(conferenceHref)
            const attendeeUrl = `http://localhost:8001${conferenceHref}attendees/`;
            console.log(attendeeUrl, json)
            const fetchConfig = {
                method: "POST",
                body: json,
                headers: {
                    "Content-Type": "application/json",
                },
            };
            // make fetch
            const newResponse = await fetch(attendeeUrl, fetchConfig);
            if (newResponse.ok) {
                formTag.reset();
                const newAttendee = await newResponse.json();
                console.log(newAttendee);
                formTag.classList.add("d-none");
                const sucess = getElementById("sucess-message");
                sucess.classList.remove("d-none");
            } else {
                console.error(newResponse.statusText)
            }
        });
    };


});
