console.log('test')
// window.addEventListener("DomContentLoaded", async () => {

//     const statesUrl = "http://localhost:8000/api/states/";
//     console.log(statesUrl)

//         const response = await fetch(statesUrl);
//         console.log(response)
//     try {
//         if (!response.ok) {
//             throw new Error ("Network response issue or something");
//         } else {
//             const data = await response.json();
//             console.log(data)

//         }

//     } catch (error) {
//         console.error('error: somethings up with teh fetch', error)
//     }
// });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const states = data.states
      // Get the select tag element by its id 'state'
      const selectTag = document.getElementById("state");
        // For each state in the states property of the data
        for (const state of states) {
            // Create an 'option' element
            const option = document.createElement("option");
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = state.abbreviation;
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = state.name;
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
        console.log(data)

    }

      const formTag = document.getElementById('create-location-form');
      formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // console.log(json, 'testing');
        const locationUrl = "http://localhost:8000/api/locations/"
        const fetchConfig = {
          method: "POST",
          body: json,
          headers: {
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        }
      });

  });
// .form-select
